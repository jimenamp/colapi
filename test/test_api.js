var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server2.js');

var should = chai.should();
chai.use(chaiHttp);

describe('Pruebas Colombia', () => {
  it('Test BBVA ', (done) => {
    chai.request('http://www.bbva.com')
        .get('/')
        .end((err,res) =>{
          console.log("res",res);
          //res.should.have.status(200);
          res.header['x-amz-cf-id']=="-4si9zUCM6CkdFl_kOnxlidMMsngKG9zOwA7HQ55bXvAiKI2-8INcQ==";
          console.log("err",err);
          done();
        })

  });
  it('Test Colapi', (done) => {
    chai.request('http://localhost:3000')
        .get('/colapi/v1/users')
        .end((err,res) =>{
         //console.log("res",res.body);
          res.should.have.status(200);
          done();
        })
  });
  it('Devuelve array de usuarios ', (done) => {
    chai.request('http://localhost:3000')
        .get('/colapi/v1/users')
        .end((err,res) =>{
          res.body.should.be.a('array');
          done();
        })
  });
  it('Devuelve al menos un elemento ', (done) => {
    chai.request('http://localhost:3000')
        .get('/colapi/v1/users')
        .end((err,res) =>{
          res.body.length.should.be.gte(1);
          done();
        })
  });

  it('validar crear elemento ', (done) => {
    chai.request('http://localhost:3000')
        .post('/colapi/v1/users')
        .send('{"first_name":"Doraemon","last_name":":)","email":"kklebes2@jimdo.com","password":"Q2IzTyJm"}')
        .end((err,res,body) =>{
          console.log(res.body.msg);
      //    body.should.be.equal()
          done();
        })
      });

});
