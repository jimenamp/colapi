      var express = require('express');
      var bodyParser = require('body-parser');
      var app = express();
      var port = process.env.PORT || 3000;

      var requestJson = require('request-json');
      var URLbase = "/colapi/v1/";
      var client = requestJson.createClient("https://api.mlab.com/api/1/databases/colapidb/collections/");
      var apiKey = "?apiKey=bx_RlvpJV67TJR6msRWJUBj6k7a9tJrO";


      app.use(bodyParser.json());

      app.listen(port, function(){
       console.log("API apicol escuchando en el puerto " + port + "...");
      });


      // GET users
      app.get(URLbase + 'users',
       function(request, response) {
        client.get('users' + apiKey , function(err, res, body) {
        response.send(body);
         });
      });

      // GET users id
      app.get(URLbase + 'users/:id',
       function(request, response) {
         var id=request.params.id;
         client.get('users/'+ id + apiKey , function(err, res, body) {
          response.send(body);
         });
      //   var email = datos.email;

         console.log("response" + response.email);

      });

      // GET accounts
      app.get(URLbase + 'accounts',
       function(request, response) {
        client.get('accounts' + apiKey , function(err, res, body) {
        response.send(body);
         });
      });


      // GET  accounts for user
      app.get(URLbase + 'users/:user_id/accounts',
       function(request, response) {
         var userId=request.params.user_id;
         var queryParam = '&q={"user_id": "'+userId+'"}';
         client.get('accounts' + apiKey + queryParam, function(err, res, body) {
           response.send(body);
         });
      });

      // GET movements
      app.get(URLbase + 'users/:user_id/accounts/:iban/movements',
       function(request, response) {
         var iban=request.params.iban;
         var queryParam = '&q={"IBAN": "'+iban+'"}';
         console.log('movements'+  apiKey + queryParam);
         client.get('movements' + apiKey + queryParam, function(err, res, body) {
          response.send(body);
         });
      });



      // Petición PUT (reg.body)
      app.put(URLbase + 'users/:id',
       function(request, response) {
          var id=request.params.id;
          var updateUser = request.body;
          client.put('users/'+ id + apiKey , updateUser, function(err, res, body) {
            response.json(body);
            response.send({"msg" : "Usuario actualizado correctamente "});
         });
        //
       });

       // Petición POST (reg.body)
       app.post(URLbase + 'users',
        function(request, response) {
           var newUser = request.body;
           client.post('users/' + apiKey ,newUser, function(err, res, body) {
             response.send({"msg" : "Usuario creado correctamente "});  });
        });

        // DELETE users id
        app.delete(URLbase + 'users/:id',
         function(request, response) {
           var id=request.params.id;
           client.delete('users/'+ id + apiKey , function(err, res, body) {
            response.send(body);
           });
        });

        // Petición POST (login)
        app.post(URLbase + 'login',
         function(request, response) {
           var loginState=true;
          updateUserLoginState(request,response,loginState);
            });

         // Petición POST (logout)
         app.post(URLbase + 'logout',
          function(request, response) {
            var loginState=false;
            updateUserLoginState(request,response,loginState);
             });


    function updateUserLoginState(request,response,loginState){

      console.log(request.body);
      var email=request.body.email;
      var pass=request.body.password;
      var queryParam = '&q={"email": "'+email+'","password": "'+pass+'"}';
            client.get('users' + apiKey + queryParam, function(err, res, body) {
             var user = body[0];
             if(user){
             if(user.email == email  && user.password == pass){
               user.logged = loginState;

               client.put('users/'+ user._id.$oid + apiKey, user, function(err, res, body) {
                 if(loginState){
                   response.send({"msg" : "login correcto","loginState" : loginState,"name" : user.first_name +" "+ user.last_name,"id":user._id.$oid});
                 }
                 else {
                   response.send({"msg" : "logout correcto ","loginState" : loginState,"name" : user.first_name +" "+  user.last_name});
                 }
               });
             }

             }
             else
             {
                response.send({"msg" : "verifica los datos ingresados "});
             }
           });
      }
