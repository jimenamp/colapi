var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var port=process.env.PORT || 3000;
var URLBase = "/colapi/v1/";
var usersFile=require("./MOCK_DATA.json");
app.listen(port);

app.use(bodyParser.json());

app.get(URLBase + 'users', function (req, res) {

       res.sendfile(usersFile);
});

app.get(URLBase +'users/:id', function (req, res) {
       var pos = req.params.id;
       res.send(usersFile[pos -1]);
});

app.post(URLBase + 'users', function (req, res) {
  console.log("Post correcto");
  console.log(req.body);
  console.log(req.body.first_name);
   var pos = req.params.id;
  var jsonId={};
  var newId = usersFile.length + 1;
  jsonId.id = newId;
  console.log(newId);
  var jsonCrear={};
  jsonCrear.id = newId;
  jsonCrear.first_name=req.body.first_name;
  jsonCrear.last_name=req.body.last_name;
  jsonCrear.email=req.body.email;
  jsonCrear.gender=req.body.country;

  console.log(jsonCrear);
  usersFile[newId] = jsonCrear;
  res.send({"msg":"Usuario creado correctamente " + newId});

});

app.put(URLBase + 'users/:id', function (req, res) {
       var pos = req.params.id;
       var updateUser = req.body;

       res.send(usersFile[pos -1]);
       var jsonActualizar=usersFile[pos -1];
       jsonActualizar.first_name=req.body.first_name;
       jsonActualizar.last_name=req.body.last_name;
       jsonActualizar.email=req.body.email;
       jsonActualizar.gender=req.body.country;
});
