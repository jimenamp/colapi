var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
var usersFile = require('./users.json');
var URLbase = "/colapi/v1/";

app.listen(port, function(){
 console.log("API apicol escuchando en el puerto " + port + "...");
});

app.use(bodyParser.json());

// GET users
app.get(URLbase + 'users',
 function(request, response) {
   console.log(URLbase);
   console.log(usersFile);
   response.send(usersFile);
});


app.get(URLbase +'users/:id', function (req, res) {
       var pos = req.params.id;
       res.send(usersFile[pos -1]);
});


// Petición GET con parámetros (req.params)
app.get(URLbase + 'users/:id/:otro',
 function (req, res) {
   console.log("GET /colapi/v1/users/:id/:otro");
   console.log(req.params);
   console.log('req.params.id: ' + req.params.id);
   console.log('req.params.otro: ' + req.params.otro);
   var respuesta = req.params;
   res.send(respuesta);
});

// Petición GET con Query String (req.query)
app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET con query string.");
   console.log(req.query.id);
   console.log(req.query.country);
   res.send(usersFile[pos - 1]);
   respuesta.send({"msg" : "GET con query string"});
});

// Petición POST (reg.body)
app.post(URLbase + 'users',
 function(req, res) {
   var newID = usersFile.length + 1;
   var newUser = {
     "id" : newID,
     "first_name" : req.body.first_name,
     "last_name" : req.body.last_name,
     "email" : req.body.email,
     "country" : req.body.country
   };
   usersFile.push(newUser);
   console.log(usersFile);
   res.send({"msg" : "Usuario creado correctamente: ", newUser});
 });

 // PUT
app.put(URLbase + 'users/:id',
  function(req, res){
    console.log("PUT /colapi/v2/users/:id");
    var idBuscar = req.params.id;
    var updateUser = req.body;
    var encontrado = false;
    for(i = 0; (i < usersFile.length) && !encontrado; i++) {
      console.log(usersFile[i].id);
      if(usersFile[i].id == idBuscar) {
        encontrado = true;
        res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
      }
    }
    if(!encontrado) {
      res.send({"msg" : "Usuario no encontrado.", updateUser});
    }
  });

// DELETE
app.delete(URLbase + 'users/:id',
 function(req, res) {

   const id = req.params.id-1;
   const reg = usersFile[id];

   if(undefined != reg){
     usersFile.splice(id,1);
     res.send(204);
  } else
    res.send(404);
});
