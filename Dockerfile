#imagen inicial a partir de la cual creamos nuestra imagen
from node

#Definimos carpeta del contenedor
WORKDIR /colapi

#Añadimos contenido del proyecto en directorio de contenedor
ADD . /colapi

#Puerto a escuchar el copntenedor
EXPOSE 3000

#Comandos para lanzar nuestra API REST `colapi`
#CMD ["npm","start"]
CMD ["node","server2.js"]
